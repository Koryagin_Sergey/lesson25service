package com.sourceit.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.Random;


public class ExampleService extends Service {

    MainActivity.OnActionGenerate onAction;

    public void setOnAction(MainActivity.OnActionGenerate onAction) {
        this.onAction = onAction;
    }

    class MyBinder extends Binder {
        ExampleService getService() {
            return ExampleService.this;
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void startDeley() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (onAction != null){
                    onAction.action(generateString());
                }
            }
        }, 5000);
    }

    public String generateString() {
        return  String.valueOf(new Random().nextInt());
    }
}
