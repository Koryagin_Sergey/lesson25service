package com.sourceit.service;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public interface OnActionGenerate {
        void  action(String string);
    }

    OnActionGenerate onActionGenerate = new OnActionGenerate() {
        @Override
        public void action(String string) {
            Toast.makeText(MainActivity.this, string, Toast.LENGTH_SHORT).show();
        }
    };

    boolean isBinder;
    ExampleService exampleService;
    Button buttonStop;
    Button buttonStart;



    ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ExampleService.MyBinder binder = (ExampleService.MyBinder) service;
            exampleService = binder.getService();
            exampleService.setOnAction(onActionGenerate);
            isBinder = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBinder = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonStart = findViewById(R.id.start);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, ExampleService.class);
//                startService(intent);
                Toast.makeText(MainActivity.this, exampleService.generateString(), Toast.LENGTH_LONG).show();
            }
        });


        Button buttonStop = findViewById(R.id.stop);
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, ExampleService.class);
//                stopService(intent);
                exampleService.startDeley();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, ExampleService.class);
        bindService(intent, conn, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(conn);
    }
}
